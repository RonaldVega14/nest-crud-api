import { json, Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/user";

export const getUsers = async (req: Request, res: Response): Promise<Response> => {
    try {
        const users = await getRepository(User).find();
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.status(505).json({ msg: 'Server not responding' });
    }
}

export const getUser = async (req: Request, res: Response): Promise<Response> => {
    try {
        const user = await getRepository(User).findOne(req.params.id);
        return res.json(user);
    } catch (error) {
        console.log(error);
        return res.status(505).json({ msg: 'Server not responding' });

    }
}

export const createUser = async (req: Request, res: Response): Promise<Response> => {
    try {
        const newUser = getRepository(User).create(req.body);
        const results = await getRepository(User).save(newUser);
        return res.json(results);
    } catch (error) {
        return res.status(505).json({ msg: 'Server not responding' })
    }
}

export const updateUser = async (req: Request, res: Response): Promise<Response> => {
    try {
        const user = await getRepository(User).findOne(req.params.id);
        if (user) {
            getRepository(User).merge(user, req.body);
            const results = await getRepository(User).save(user);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'Not user found' });
    } catch (error) {
        return res.status(505).json({ msg: 'Server not responding' })
    }
}

export const deleteUser = async (req: Request, res: Response): Promise<Response> => {
    try {
        const results = await getRepository(User).delete(req.params.id)
        return res.json(results);
    } catch (error) {
        return res.status(505).json({ msg: 'Server not responding' })
    }
}